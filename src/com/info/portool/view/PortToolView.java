package com.info.portool.view;

import com.info.portool.utils.CmdUtil;
import com.info.portool.utils.InputUtility;

public class PortToolView {
    public void showView() {
        boolean flag = true;
        char key;
        do {
            System.out.println("1-查询指定端口信息 2-关闭端口程序 3-退出 请选择(1-3):");
            key = InputUtility.readMenuSelection();
            switch (key) {
                case '1':
                    findPortInfo();
                    break;
                case '2':
                    killPidInfo();
                    break;
                case '3':
                    System.out.println("请选择是或否（Y/N）");
                    char res = InputUtility.readConfirmSelection();
                    if (res == 'Y') {
                        flag = false;
                    }
                    break;
            }
        } while (flag);

    }

    private void killPidInfo() {
        System.out.println("");
        System.out.println("请输入您要删除的Pid信息");
        int pid = InputUtility.readInt();
        CmdUtil.killByPid(pid);
        InputUtility.readReturn();
    }

    private void findPortInfo() {
        System.out.println("");
        System.out.println("请输入您要查询的端口号");
        int port = InputUtility.readInt();
        CmdUtil.findInfoByPort(port);
        InputUtility.readReturn();
    }
}
