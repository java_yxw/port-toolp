package com.info.portool.main;

import com.info.portool.view.PortToolView;

public class Main {
    public static void main(String[] args) {
        PortToolView portToolView = new PortToolView();
        portToolView.showView();
    }
}
