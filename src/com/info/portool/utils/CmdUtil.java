package com.info.portool.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CmdUtil {

    /** 根据端口号查询对应信息
     * @author yixiaowei
     * @param port 端口号
     * @return
     */
    public static void findInfoByPort(int port){
        try {
            // 执行cmd命令（核心功能）
            Process pro = Runtime.
                    getRuntime().
                    exec("cmd /c netstat -ano|findstr " + port);

            // 定义读取信息的流对象
            BufferedReader br = new BufferedReader(new InputStreamReader(pro.
                    getInputStream(),
                    "gb2312"));

            String msg;
            StringBuffer info = new StringBuffer();
            // 读取数据
            while ((msg = br.readLine()) != null) {
                info.append(msg + "\n");
            }

            // 判断是否查到数据
            if (info.toString().equals("")) {
                System.out.println("很抱歉，没有查到相关数据，可能是您的端口号输入错误 或 当前端口没有被占用，请重新查找");
            } else {
                // 输出结果
                System.out.println(info.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        };
    }

    /** 根据pid关闭对应程序
     * @author yixiaowei
     * @param pid pid信息
     * @return
     */
    public static void killByPid(int pid){
        try {
            // 执行cmd命令（核心功能）
            Process pro = Runtime.
                    getRuntime().
                    exec("cmd /c taskkill /f /pid " + pid);

            // 定义读取信息的流对象
            BufferedReader br = new BufferedReader(new InputStreamReader(pro.
                    getInputStream(),
                    "gb2312"));

            String msg;
            StringBuffer info = new StringBuffer();
            // 读取数据
            while ((msg = br.readLine()) != null) {
                info.append(msg + "\n");
            }

            // 判断是否查到数据
            if (info.toString().equals("")) {
                System.out.println("关闭失败，没有查询到对应的pid信息，请检查pid是否输入错误");
            } else {
                // 输出结果
                System.out.println(info.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        };
    }
}
